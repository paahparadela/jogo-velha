package com.itau.gamevelha.elemento;

public enum Valor {
	
	Vazio(" "),
	X("X"),
	O("O");
	
	String valor;
	
	Valor(String valor){
		this.valor = valor;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	

}
