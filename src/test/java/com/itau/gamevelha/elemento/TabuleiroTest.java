package com.itau.gamevelha.elemento;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TabuleiroTest {
	
	@Test
	public void deveRetornarTabuleiroVazio() {
		Tabuleiro tabuleiro = new Tabuleiro();
		
		tabuleiro.inicializar();
		
		assertEquals(tabuleiro.toString(), "[ ][ ][ ]\n[ ][ ][ ]\n[ ][ ][ ]\n");
	}

}
